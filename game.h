/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file       game.h
 * \author     Antoine Chavee 
 * \brief      Définition de la structure d'une partie pour stocker les informations des joueurs de cette partie
 */

#ifndef GAME_H
#define GAME_H

typedef struct {
    int idGame; /*!< Numéro id de la game  */
    int nbJoueur; /*!< Nombre de joueur maximum autorisé 2 pour chaque partie */
    int round; /*!< Indique le numéro du round */
    int time;
    int idplayer1; /*!< Identification du joueur 1 via un numéro d'id*/
    int idplayer2; /*!< Identification du joueur 2 via un numéro d'id*/
    int socketplayer1; /*!< Identification du socket du joueur 1 via un numéro de socket*/
    int socketplayer2; /*!< Identification du socket du joueur 2 via un numéro de socket*/
    int keyplayer1; /*!< Clé unique du joueur 1*/
    int keyplayer2; /*!< Clé unique du joueur 2*/
    
} Game;

#endif /* GAME_H */

