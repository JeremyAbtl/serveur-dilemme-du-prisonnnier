/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file       protocole.c
 * \author     Antoine Chavee 
 * \brief      Fichier concernant les protocoles
 */

#include <stdio.h>
#include "protocole.h"
#include "rules.h"

// déclaration des variables globales pour les utilisées dans toutes les fonctions
Game uneGame;
DilemData tabData[256];
//i est la taille de mon tableau tabData qui est la table avec les données des joueurs
int i = 0;

/**
 * \brief Fonction qui regarde le numéros de protocole de la structure de donnée et fais une action en fonction
 * numProtocole = 80 -> création d'une nouvelle game et connection d'un nouveau joueur
 * numProtocole = 33 -> réception des choix des joueurs
 * numProtocole = 0 -> affichage coté serveur
 * @param structure DilemData
 * @param structure connection_t
 */

void readProtocole(DilemData dilemdata,connection_t *connection){
    if(dilemdata.numPr == 0){  
        display(dilemdata);
    }
     if(dilemdata.numPr == 80){
        createGame(dilemdata,connection);
    }
    if(dilemdata.numPr == 33){
       gameChoice(dilemdata);
    }
}

/**
 * \brief Fonction d'affichage
 * @param une structure DilemData
 */
void display(DilemData dilemdata){

    printf("display Protocole-----------------------------------------------------------\n");
        printf("idPlayer : %i\n", dilemdata.idPlayer);
        printf("idGame: %i\n", dilemdata.idgame);
        printf("Action si 1 trahison et si 0 coopère: %i\n", dilemdata.action);
        printf("nbProtocole : %i\n", dilemdata.numPr);
  
        printf("----------------------------------------------------------------\n");
        

}
/**
 * \brief Fonction créant la game 
 * @param une structure DilemData
 * @param une structure connection_t
 * pour renvoyer au client que la game est bien créer et qu'il c'est connecté a celle-ci
 */
void createGame(DilemData dilemdata,connection_t *connection){
   
    uneGame.nbJoueur++;
      //condition pour avoir 2 joueurs dans une game 
    if(uneGame.nbJoueur <= 2){ 
        //si un seul joueur viens de se connecter on l'enregistre dans la game et on renvoie au joueur un que la game ne peut start car 1 joueur 
        if(uneGame.nbJoueur == 1){
            //récupéreation du sockfd du joueur un dans la game     
            uneGame.idplayer1 = dilemdata.idPlayer;   
            //récupéreation du sockfd du joueur un dans la game
            uneGame.socketplayer1 = connection->sockfd;
             //indication de début de game 0(1 joueur) 1 (2joueur)
            dilemdata.gameStart = 0;
        }if(uneGame.nbJoueur == 2){
            //deuxième joueur connecter
            uneGame.idplayer2= dilemdata.idPlayer;
             //indication game prête
            dilemdata.gameStart = 1;
            uneGame.socketplayer2 = connection->sockfd;
            //game full donc changement de l,id game pour la prochaine partie
            uneGame.idGame++;
        }
    }
    dilemdata.idgame = uneGame.idGame;
    //envoie de donnée au joueur qui viens de ce connecter
    write(connection->sockfd,&dilemdata,sizeof(DilemData));
    
    if(dilemdata.gameStart == 1){   
      
    // si game full envoie au premier joueur connecter les données et que la game peut commencer
        
    write(uneGame.socketplayer1,&dilemdata,sizeof(DilemData));
    }
 
}

/**
 * \brief Fonction de récupération des choix du joueur 
 * @param une structure DilemData 
 */
void gameChoice(DilemData dilemdata){
  
   //i taille tableaux = 0 alors pas de données déjà reçus
    if(i==0){
    
    // ajout de la première donnée recus au tableaux
    tabData[i] = dilemdata; 
    }
    // i taille tableaux = 1 un joueur a déjà envoyer des données
    if(i==1){
        //récupère les données déjà reçus
     DilemData unDilemdata = tabData[i-1];
     //on compare les id des données reçus pour savoir si c'est le même joueur qui a envoyé des données
     if(unDilemdata.idPlayer != dilemdata.idPlayer){
     //si ce n'est pas le même joueurs on l'ajoute a notre tableaux de données
     tabData[i] = dilemdata;
     }
    }

  i++;
  
   //si les deux joueurs on répondue
    if(i == 2){
   //on donne le tableaux de données et la taille du tableaux au règle du jeux
     i = gameRules(tabData,i);

    }
    
}

