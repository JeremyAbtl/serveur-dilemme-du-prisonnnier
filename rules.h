/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file       rules.h
 * \author     Antoine Chavee 
 * \brief      fichiers contenant les signatures des fonction du fichier rules.c
 */

#include "dilemData.h"
#include "game.h"
#include "protocole.h"

#ifndef RULES_H
#define RULES_H

/**
 * \brief Fonction qui décide des régle du jeux de la partie 
 * @param un tableaux de struct Dilemdata et une taille i
 */
int gameRules(DilemData tabData[],int i);

#endif /* RULES_H */

