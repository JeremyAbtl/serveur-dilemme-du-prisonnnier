/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file       protocole.c
 * \author     Antoine Chavee 
 * \brief      Fichier contenant les signatures des fonctions du fichier protocole.c
 */

#include "dilemData.h"
#include "game.h"
#include "srvcxnmanager.h"

//Création d'une structure protocole pour dirigé les données en fonction de leur numéros de protocole

#ifndef PROTOCOLE_H
#define PROTOCOLE_H


typedef struct {
    int numProtocole; /*!< Contient le numéro de protocole        */
} Protocole;

/**
 * \brief Fonction qui regarde le numéros de protocole de la structure de donnée et fais une action en fonction
 * numProtocole = 80 -> création d'une nouvelle game et connection d'un nouveau joueur
 * numProtocole = 33 -> réception des choix des joueurs
 * numProtocole = 0 -> affichage coté serveur
 * @param structure DilemData
 * @param structure connection_t
 */
void readProtocole(DilemData dilemdata,connection_t *connection);

/**
 * \brief Fonction d'affichage
 * @param une structure DilemData
 */
void display(DilemData dilemdata);

/**
 * \brief Fonction créant la game 
 * @param une structure DilemData
 * @param une structure connection_t
 * pour renvoyer au client que la game est bien créer et qu'il c'est connecté a celle-ci
 */
void createGame(DilemData dilemdata,connection_t *connection);

/**
 * \brief Fonction de récupération des choix du joueur 
 * @param une structure DilemData 
 */
void gameChoice(DilemData dilemdata);


#endif /* PLAYER_H */