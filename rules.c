/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file       rules.c
 * \author     Antoine Chavee 
 * \brief      Fichier concernant les règles du jeu
 */

#include "rules.h"
//récupération des données de la game
extern Game uneGame;

/**
 * \brief Fonction qui décide des régle du jeux de la partie 
 * @param un tableaux de structure Dilemdata
 * @param taille du tableau
 */
int gameRules(DilemData *tabData,int i){

    //récupération des deux données recus dans une structure DilemData et instanciation de la mise
    int mise = 50;
   
    DilemData player1 = tabData[0];
    DilemData player2 = tabData[1];
   //Condition pour comparer les choix des joueurs et repondre en conséquence
    if(player1.action == player2.action){
        if(player1.action == 1){
            
            //les deux joueur on colaboré
            player1.gain = mise/2;
            player2.gain = mise/2;
            
            player1.resultChoice = 1;
            player2.resultChoice = 1;
            
        }else{
            //les deux joueur on trahis
            
            player1.gain=0;
            player2.gain=0;
            
             player1.resultChoice = 2;
             player2.resultChoice = 2;
             
        }
    
    }if(player1.action > player2.action){
     //le joueur 1 a trahis le joueur 2
     player1.gain=mise;
     player2.gain=0;
     
      player1.resultChoice = 3;
      player2.resultChoice = 4;

    }if(player1.action < player2.action){
    //le joueur 2 a trahis le joueur 1
    player1.gain=0;
    player2.gain=mise;
    
     player1.resultChoice = 3;
     player2.resultChoice = 4;
    }
    //calcule de la cagnotte des 2 joueurs après l'application des règles
    player1.cagnotte = player1.cagnotte +  player1.gain;
    player2.cagnotte = player2.cagnotte +  player2.gain;
    
    //passage au round suivant de la game et envoie de celle-ci au joueur
    player1.round++;
    player2.round++;
    
    uneGame.round++;
     //affichage des données a envoyer au joueur 
    printf("joueur 1 --------- \n");
    printf("id player 1: %d \n",player1.idPlayer);
    printf("gain joueur 1: %d \n",player1.gain);
    printf("cagnotte joueur 1: %d \n",player1.cagnotte);
    printf("round joueur 1: %d \n",player1.round);
    
    //envoye données au joueur 1
     write(uneGame.socketplayer1,&player1,sizeof(DilemData));
     
    printf("joueur 2 --------- \n");
    printf("id player 2: %d \n",player2.idPlayer);
    printf("gain joueur 2: %d \n",player2.gain);
    printf("cagnotte joueur 2: %d \n",player2.cagnotte);
    printf("round joueur 1: %d \n",player1.round);
    if(uneGame.round == 5 ){
    player1.resultChoice == 5;
    player2.resultChoice == 5;
    }
    //envoye des données au joueur 2
     write(uneGame.socketplayer2,&player2,sizeof(DilemData));
  

    return 0;
}